var token;


// Restricts input for each element in the set of matched elements to the given inputFilter.
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

$(document).ready(function() {
  $('[name="cantidad"]').inputFilter(function(value) {
    return /^\d*$/.test(value);
  });
});


$( '[name="cantidad"]' ).focusout(function() {
    VerificarDispararLB();
});  
function VerificarDispararLB(){
    if( $('[name="cantidad"]').val()!='' && $('[name="cantidad"]').val() < 30){
         $.fancybox.open({
            src  : '#hidden-content',
            type : 'inline'
          });
         return false;
    }
    return true;

}
function verificarFormulario() {

    if(!VerificarDispararLB())
        return;
	
    $('#botonForm').val('Enviando...');

	var campos = ['cantidad', 'nombre', 'email'];   

	for (var i = 0; i < campos.length; i++) {
		
        document.getElementById(campos[i]).classList.remove('error');

        if($('[name="'+campos[i]+'"]').val() == '') {
            document.getElementById(campos[i]).placeholder = document.getElementById( campos[i] ).getAttribute( 'data-error' );
            document.getElementById(campos[i]).classList.add('error');
            $('[name="'+campos[i]+'"]').focus();
            $('#botonForm').val('Enviar');
			return;
		}
	}
     
    if(!validateEmail($('[name="email"]').val())) {
		document.getElementById("email").classList.add('error');
		$('#botonForm').val('Enviar');
		return;
	}
    /*
	grecaptcha.execute('6LerAasUAAAAAI-OoGtZLE_ITPm_E9p7PfMFIxL5', {action: 'homepage'}).then(function(t) {
		token = t;
		$('.msj-error').removeClass('d-block');
        enviarFormulario();
    });*/
    enviarFormulario();
}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function enviarFormulario() {
	$.ajax({
        url: 'php/mail.php', 
        method: 'POST',
        dataType: 'json',
        data: {
        	token: token,
        	cantidad: $('[name="cantidad"]').val(),
        	nombre: $('[name="nombre"]').val(),
        	telefono: $('[name="telefono"]').val(),
        	email: $('[name="email"]').val(),
        	direccion: $('[name="direccion"]').val()
        },
        success: function(r){
            if(r.resultado == 201) {
                document.getElementById("formulario").reset();
                $('#botonForm').val('Enviado Exitosamente');
                gtag('event', 'conversion', {'send_to': 'AW-792981691/pbbACJXX5bABELvhj_oC'})
            } else {
            	alert(r.mensaje);
                $('#botonForm').val('Error!');
            }


            setTimeout(function(){
                  $('#botonForm').val('Enviar');
            }, 5000);
        }
    });
}