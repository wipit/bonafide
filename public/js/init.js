$(document).ready(function(){
  	$(".Filtro-TipoVenta .FiltroBtn").click(function(event){
    	event.preventDefault();
    	$( this ).toggleClass( "active" );
	});

  if( $(".encabezado").length > 0 ) {
    $('.encabezado').owlCarousel({
      // animateOut: 'fadeOut',
      loop: true,
      margin:0,
      dots:true,
      center:true,
      items:1,
      autoplay: true,
      nav:false,
      smartSpeed:600,
      responsiveClass:true,
      dotsContainer: '#dots'
    });
  }

  $( ".icon-menu-mobile" ).click(function() {
    $( ".menu-mobile-open" ).fadeIn(300); 
  });

  $(".cierra-menu").click(function() {
    $( ".menu-mobile-open" ).fadeOut(200);
  });

  $("#Cerrar-Notificacion").click(function() {
    $( ".Aviso-Notificacion" ).fadeOut(200);
  });
    wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
  wow.init();
  $('.collapse').collapse({
    toggle: false
  });
});
//ramdom
function random(owlSelector){
    owlSelector.children().sort(function(){
        return Math.round(Math.random()) - 0.5;
    }).each(function(){
      $(this).appendTo(owlSelector);
    });
  }
   
   var $owl = $( '.owl-carousel' );
   $owl.on('initialize.owl.carousel', function(event){
	   var selector = $('.owl-carousel');
	   // random(selector);
	});
/* productos home */
$(document).ready(function() {
 "use strict";
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
	lazyLoad:true,
	autoplay:true,
    nav:true,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
});
	});
/* productos productos */
$(document).ready(function() {
	"use strict";
  $('.owl-carousel-productos').owlCarousel({
    loop:true,
    margin:0,
	lazyLoad:true,
	autoplay:true,
    nav:true,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
});
	});
$(document).ready(function(){
	$('#encontra').change(function(){
		$('#los-productos').html('<p style="padding: 4em;">Cargando...</p>');
		var categoria = $(this).val();
		$('html,body').animate({scrollTop: $('#select-productos').offset().top}, 800);
		$.ajax({
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
	        type: "POST",
	        url: '/cargar',
	        data: {
	        	categoria: categoria
	        },
	        success: function(data) {
	            $('#los-productos').html(data);
	        }
	    });
	});
});
$(document).ready(function(){
	$('.BlockInfo.franquicias').removeClass('hide').addClass('activo');
	$('.tab-franquicias').removeClass('hide').addClass('visi');
	$('.empresas-banner').addClass('hide');
	$('.tab-empresas').addClass('hide');
	$('.franquicias-banner').removeClass('hide').addClass('visi');
	$('.tab-petroleras').addClass('hide');
	$('.petrolera-banner').addClass('hide');
	$('.franquicias-bottom').removeClass('hide').addClass('visi');
	$('.BlockInfo.empresas').click(function(){
		$('html,body').animate({scrollTop: $('#scroll_to').offset().top}, 500);
		$(this).addClass('activo');
		$('.BlockInfo.franquicias').removeClass('activo');
		$('.BlockInfo.petroleras').removeClass('activo');
		$('.tab-empresas').removeClass('hide').addClass('visi');
		$('.empresas-banner').removeClass('hide').addClass('visi');
		$('.tab-franquicias').addClass('hide');
		$('.franquicias-banner').addClass('hide');
		$('.tab-petroleras').addClass('hide');
		$('.franquicias-bottom').addClass('hide');
		$('.petrolera-banner').addClass('hide');
	});
	$('.BlockInfo.franquicias').click(function(){
		$('html,body').animate({scrollTop: $('#scroll_to').offset().top}, 500);
		$(this).addClass('activo');
		$('.BlockInfo.empresas').removeClass('activo');
		$('.BlockInfo.petroleras').removeClass('activo');
		$('.tab-empresas').addClass('hide');
		$('.empresas-banner').addClass('hide');
		$('.tab-franquicias').removeClass('hide').addClass('visi');
		$('.franquicias-banner').removeClass('hide').addClass('visi');
		$('.franquicias-bottom').removeClass('hide').addClass('visi');
		$('.tab-petroleras').addClass('hide');
		$('.petrolera-banner').addClass('hide');
	});
	$('.BlockInfo.petroleras').click(function(){
		$('html,body').animate({scrollTop: $('#scroll_to').offset().top}, 500);
		$(this).addClass('activo');
		$('.BlockInfo.franquicias').removeClass('activo');
		$('.BlockInfo.empresas').removeClass('activo');
		$('.tab-empresas').addClass('hide');
		$('.empresas-banner').addClass('hide');
		$('.tab-franquicias').addClass('hide');
		$('.franquicias-banner').addClass('hide');
		$('.franquicias-bottom').addClass('hide');
		$('.tab-petroleras').removeClass('hide').addClass('visi');
		$('.petrolera-banner').removeClass('hide').addClass('visi');
	});
});

$(document).ready(function(){
	$('.sub-login').hide();
	$('.franquicias-cerrar-x').hide();
	$('.franquicias-abrir').click(function(){
		$('.sub-login').show();
		$(this).hide();
		$('.franquicias-cerrar-x').show();
	});
	$('.franquicias-cerrar-x').click(function(){
		$('.franquicias-abrir').show();
		$('.sub-login').hide();
		$(this).hide();
		
	});
});

$(document).ready(function(){
	$('#FormContactoSelect').on('change', function() {
		$('#asunto').val(this.value);
	});
	$('#contacto-bonafide').validate();
});

$(document).ready(function(){
	"use strict";
			  $('.BgHeader-Inicio').css('background', 'url(./images/banners/navidad-inicio.jpg) no-repeat');
			  $('.BgHeader-Inicio').css('background-size', '100%');
	// window.onresize = function(){ location.reload(); } //banners responsive
	if ($(window).width() <= 1024) {  
              $('.BgHeader-Inicio').css('background', 'url(./images/banners/navidad-1024x452.jpg) no-repeat');
			  $('.BgHeader-Inicio').css('background-size', '100%');
			  // window.onresize = function(){ location.reload(); }
       }
		if ($(window).width() <= 768) {  
			  $('.BgHeader-Inicio').css('background', 'url(./images/banners/navidad-768x339.jpg) no-repeat');
			  $('.BgHeader-Inicio').css('background-size', '100%');
			// window.onresize = function(){ location.reload(); }
		   }  
	if ($(window).width() <= 480) {  
			  $('.BgHeader-Inicio').css('background', 'url(./images/banners/navidad-480x855-01.jpg) no-repeat');
			  $('.BgHeader-Inicio').css('background-size', '100%');
			  $('.Header').css('height', '100vh');
		// window.onresize = function(){ location.reload(); }
		   } 
	
});
