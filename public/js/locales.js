google.maps.Map.prototype.clearMarkers = function() {
    if(infow) {
      infowindow.close();
    }
    
    for(var i=0; i<this.markers.length; i++){
      this.markers[i].set_map(null);
    }
  };

function completo(){
	$(".locals .locals_c .steps").hide();
	/*map.setCenter(results[0].geometry.location);
	map.setZoom(14);*/
}

var geocoder;
var map;
var total;
var infoWindows = [];

function closeAllInfoWindows() {
  for (var i=0;i<infoWindows.length;i++) {
     infoWindows[i].close();
  }
}

function initialize(){
	geocoder = new google.maps.Geocoder();
	var mapOptions = {
		zoom: 12
	}
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	
	
	var address = "Buenos Aires, CABA";
	geocoder.geocode( { 'address': address}, function(results, status) {
		if(status == google.maps.GeocoderStatus.OK){
			map.setCenter(results[0].geometry.location);
		}else{
			console.log("Geocode was not successful for the following reason: " + status);
		}
	});
	
	loadInfo();
	fillUbicacion();
	fillUbicacion2();
	
	total = myInfo.length;
}

function search(){
	var ubicacion = $("#ubicacion").val();
	var localidad = $("#localidad").val();
	
	if(ubicacion!=""&&localidad!=""){
		
		var address = localidad+", "+ubicacion;
		
		$(".locals .locals_c .steps").hide();
		
		geocoder.geocode( { 'address': address}, function(results, status) {
			if(status == google.maps.GeocoderStatus.OK){
				map.setCenter(results[0].geometry.location);
				map.setZoom(14);
			}else{
				console.log("Geocode was not successful for the following reason: " + status);
			}
		});
	
	}
}

function addMarker(myAddress, localInfo, myName, myImage, horarios, lat, lang){
	
	var address = myAddress+", Argentina";
	
	var mode = "";
	
	var popupString = '<div id="content" class="map-popup">';
	popupString = popupString + '<p id="firstHeading" class="firstHeading">'+myName+'</p>';
	popupString = popupString + '<div id="bodyContent">';
	popupString = popupString + '<p class="myInfo">';
	popupString = popupString + myAddress + "<br><br>" + horarios;
	popupString = popupString + '</p>';
	popupString = popupString + '</div>';
	popupString = popupString + '</div>';
	
	var infowindow = new google.maps.InfoWindow({content: popupString});
	infoWindows.push(infowindow); 
			
	var image = 'images/locales/icons/'+myImage+'.png';
	
	if(lat==0&&lang==0){
		
		mode = "geo";
	
		geocoder.geocode( { 'address': address}, function(results, status) {
			if(status == google.maps.GeocoderStatus.OK){

				var marker = new google.maps.Marker({
					map: map,
					animation: google.maps.Animation.DROP,
					icon: image,
					position: results[0].geometry.location
				});
				
				google.maps.event.addListener(marker, 'click', function() {
					closeAllInfoWindows();
					infowindow.open(map,marker);
				});
				
				var locationl = results[0].geometry.location;
				console.log(locationl.toString());
				
			}else{
				
				console.log("Geocode was not successful for the following reason: " + status + " " +address);
				
			}
		});
	
	}else{
		
		mode = "no-geo";
		
		var marker = new google.maps.Marker({
			map: map,
			/*animation: google.maps.Animation.DROP,*/
			icon: image,
			position: {lat: lat, lng: lang}
		});
		
		/*google.maps.event.addListener(marker, 'click', function() {
			closeAllInfoWindows();
			infowindow.open(map,marker);
		});*/
		google.maps.event.addListener(marker, 'click', function (marker, i) {
					/*if ($('#info-container').css('display') == 'block') {
						$('#info-container').css('display', 'none');
					} else {*/
						$('#info-container').css('display', 'block');
						$('#map-imagen').html( "<img src='images/locales/"+localInfo+"' class='img-responsive'>");
						$('#map-texto').html( "<h2>"+myName+"</h2><div class='texto'>"+ myAddress + "<br><br>" + horarios +"</div>");
					/*}*/
				});
		
	}
	
	return mode;
}

function fillUbicacion(){
	var code = '<option value="" disabled selected style="display: none;">Ubicación</option>';
	for(i = 0; i < ubicacion.length; i++){ 
		code += ('<option value="'+ubicacion[i][0]+', '+ubicacion[i][2]+'">'+ubicacion[i][1]+'</option>');
	}
	$("#ubicacion").html(code);
}
function fillUbicacion2(){
	var code = '<option value="" disabled selected style="display: none;">Ubicación</option>';
	for(i = 0; i < ubicacion.length; i++){ 
		code += ('<option value="'+ubicacion[i][0]+', '+ubicacion[i][2]+'" onmouseover="highlight(\''+ubicacion[i][3]+'\');">'+ubicacion[i][1]+'</option>');
	}
	$("#ubicacion_step").html(code);
}

function fillLocalidades(){
	var currentUbicacion = $("#ubicacion").val();
	var code = '<option value="" disabled selected style="display: none;">Localidad</option>';
	for(i = 0; i < localidades.length; i++){
		if(currentUbicacion==localidades[i][0]){
			code += ('<option value="'+localidades[i][1]+'">'+localidades[i][1]+'</option>');
		}
	}
	$("#localidad").html(code);
}

var loading = 0;
var x = 0;
function loadInfo(){
	var myVar = setInterval(function(){
		
		var mode = addMarker(myInfo[x][0]+', '+myInfo[x][1], myInfo[x][3], myInfo[x][4], myInfo[x][5], myInfo[x][6], myInfoLat[x][0], myInfoLat[x][1]);
		loading++;
		var progres = Math.floor((loading*100)/total);
		$("#loading").html(progres);
		/*console.log(mode);*/
		
		if((x+1)==myInfo.length){
			clearInterval(myVar);
			$(".loading").fadeOut(400);
		}
		
		x++;
		
		if(mode=="geo"){
			clearInterval(myVar);
			setTimeout(function(){loadInfo();}, 1200);
		}
		
	}, 10);
}

function openInfo(code){
	$("#info").html(code);
}

/*STEPS*/
var ubicacion_step = "";
var localidad_step = "";
var ubicacion_step_name = "";
function step1(){
	$(".steps .step1").hide();
	$(".steps .step2").show();
}
function step2(){
	$(".steps .step2").hide();
	$(".steps .step3").show();
	ubicacion_step = $("#ubicacion_step").val();
	fillLocalidades_step();
	
	ubicacion_step_name = ubicacion_step.replace(', Argentina','');
	ubicacion_step_name = ubicacion_step_name.replace(', Uruguay','');
	$("#ubicacion_step3").html(ubicacion_step_name);
	
	if(ubicacion_step == "Ciudad Autónoma de Buenos Aires, Argentina" || ubicacion_step == "Buenos Aires, Argentina"){
		$('.volver').css('margin-right', '31.5%');
	}else{
		$('.volver').css('margin-right', 'auto');
	}
}
function step3(localidad){
	$(".steps .step3").hide();
	$(".steps .step4").show();
	localidad_step = localidad;
	
	$("#en_step").html(ubicacion_step_name);
	fillLocales_step();
}

function volverStep3(){
	$(".steps .step2").show();
	$(".steps .step3").hide();
	$(".step .map .capa").fadeOut(200);
	$("#ubicacion_step").val("");
}
function volverStep4(){
	$(".steps .step3").show();
	$(".steps .step4").hide();
}

function fillLocalidades_step(){
	var code = '';
	for(i = 0; i < localidades.length; i++){
		if(ubicacion_step==localidades[i][0]){
			code += ('<div class="localidad button" onclick="step3(\''+localidades[i][1]+'\');">'+localidades[i][1]+'</div>');
		}
	}
	$("#localidades_step").html(code);
}

function fillLocales_step(){
	var code = '';
	for(i = 0; i < myInfo.length; i++){
		if(ubicacion_step==myInfo[i][1] && localidad_step==myInfo[i][2]){
			code += ('<div class="local"><h5>'+myInfo[i][2]+'</h5><p>'+myInfo[i][0]+'<br><br>'+myInfo[i][6]+'</p><div class="ver button" onclick="centerDir(\''+myInfo[i][2]+','+myInfo[i][1]+','+myInfo[i][0]+'\')">Ver en el mapa</div></div>');
		}
	}
	$("#locales_step").html(code);
}

function centerDir(dir){
	var address = dir;
	
	$(".locals .locals_c .steps").hide();
	
	geocoder.geocode( { 'address': address}, function(results, status) {
		if(status == google.maps.GeocoderStatus.OK){
			map.setCenter(results[0].geometry.location);
			map.setZoom(16);
		}else{
			console.log("Geocode was not successful for the following reason: " + status);
		}
	});
}

function highlight(id){
	$(".step .map .capa").stop().fadeOut(200);
	$(".step .map .capa."+id).stop().fadeIn(200);
}