<?php
	Route::get('/fix', 'PagesController@fix')->name('fix');
	Route::get('/', 'PagesController@index')->name('index');
	Route::get('/nosotros', 'PagesController@nosotros')->name('nosotros');
	Route::get('/locales', 'PagesController@locales')->name('locales');
	Route::get('/productos/{nombre?}', 'PagesController@productos')->name('productos');
	Route::get('/negocios/{nombre?}', 'PagesController@negocios')->name('negocios');
	Route::get('/novedades_', 'PagesController@novedades')->name('novedades');
	Route::get('/contacto/{tema?}', 'PagesController@contacto')->name('contacto');
	Route::post('/contacto', 'PagesController@enviar')->name('enviar');
	Route::post('/cargar', 'PagesController@cargar')->name('cargar');
	Route::get('/novedad/{novedad}', 'PagesController@novedad')->name('novedad');