<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentacion extends Model {
    protected $table = 'presentaciones';
    protected $guarded = [];

    public function producto() {
        return $this->belongsTo('App\Producto');
    }
}
