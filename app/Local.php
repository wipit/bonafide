<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    protected $table = 'locales';

    public function localidad() {
        return $this->belongsTo('App\Localidad');
    }
}
