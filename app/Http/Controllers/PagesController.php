<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactoRecibido;

class PagesController extends Controller {
    public function fix() {
        $productos = \App\Producto::all();
        foreach($productos as $producto) {
            \DB::insert('insert into categoria_producto (categoria_id, producto_id) values (?, ?)', [$producto->categoria_id, $producto->id]);
        }
        echo 'ok';
    }

    public function index() {
        $destacados = \App\Destacado::where('visible', 1)->orderBy('orden', 'asc')->get();
        $productos = \App\Producto::where('home', 1)->where('visible', 1)->inRandomOrder()->get();
        return view('index')->with(['destacados' => $destacados, 'productos' => $productos]);
    }

    public function nosotros() {
        return view('nosotros');
    }

    public function locales() {
        $provincias = \App\Provincia::all();
        $localidades = \App\Localidad::orderBy('localidad', 'asc')->get();
        $locales = \App\Local::with(['localidad', 'localidad.provincia'])->orderBy('id', 'asc')->get();
        return view('locales')->with(['provincias' => $provincias, 'localidades' => $localidades, 'locales' => $locales]);
    }

    public function productos($nombre = null) {
        $categorias = \App\Categoria::where('visible', 1)->selectRaw('IF(LOCATE("FAMILIA", nombre), SUBSTR(nombre, 9, LENGTH(nombre) - 8), nombre) as nombre, id')->where('parent_id', 0)->orderBy('nombre', 'asc')->get();
        $nombre = $nombre == 'chocolate' ? 'rama' : $nombre;
        $categoria = $nombre ? \App\Categoria::where('slug', $nombre)->first() : null;
        if($categoria) {
            $productos = \App\Producto::where('categoria_id', $categoria->id)->where('visible', 1)->orderBy('id', 'asc')->get();
            $productos = $productos->sortBy(function($prod){
                return $prod->categoria->first()->nombre;
            })->values()->all();
        } else {
            $productos = \App\Producto::where('destacado', 1)->where('visible', 1)->orderBy('id', 'asc')->get();
            $productos = $productos->sortBy(function($prod){
                return $prod->categoria->first()->nombre;
            })->values()->all();
        }
        return view('productos')->with(['categorias' => $categorias, 'productos' => $productos, 'seleccionada' => $categoria]);
    }

    public function negocios($nombre = null) {
        return view('negocios')->with('preset', $nombre);
    }

    public function novedades() {
        $novedades = \App\Novedad::where('visible', 1)->orderBy('created_at', 'desc')->get();
        return view('novedades')->with('novedades', $novedades);
    }

    public function novedad(\App\Novedad $novedad) {
        return view('novedad')->with('novedad', $novedad);
    }

    public function contacto($tema = null) {
        return view('contacto')->with(['enviado' => false, 'tema' => $tema]);
    }

    public function cargar(Request $request) {
        $hijos = \App\Categoria::where('parent_id', $request->categoria)->pluck('id');
        if(count($hijos)) {
            $productos = \App\Producto::whereHas('categoria', function($q) use($hijos) {
                $q->whereIn('categoria_id', $hijos);
            })->where('visible', 1)->orderBy('nombre', 'asc')->get();
            // $productos = \App\Producto::where('visible', 1)->whereIn('categoria_id', $hijos)->orderBy('id', 'asc')->get();
            $productos = $productos->sortBy(function($prod){
                return $prod->categoria->first()->nombre;
            })->values()->all();
        } else {
            $productos = \App\Producto::whereHas('categoria', function($q) use($request) {
                $q->where('categoria_id', $request->categoria);
            })->where('visible', 1)->orderBy('nombre', 'asc')->get();
            // $productos = \App\Producto::where('visible', 1)->where('categoria_id', $request->categoria)->orderBy('id', 'asc')->get();
            $productos = $productos->sortBy(function($prod){
                return $prod->categoria->first()->nombre;
            })->values()->all();
        }
        return view('listado')->with(['productos' => $productos]);
    }

    public function enviar(Request $request) {
        $validatedData = $request->validate([
            'nombre' => 'required',
            'mail' => 'required|email'
        ]);
        if($request['asunto'] == 'Recursos Humanos')
            $mail = 'rhumanos@carozzicorp.com'; //'recursoshumanos@bonafide.com.ar';
        else
            $mail = 'operador6@carozzicorp.com'; //0800@bonafide.com.ar';
        $file = $request->cv ? $request->cv : '';
        Mail::to($mail)->send(new ContactoRecibido($request, $file));
        return view('contacto')->with(['enviado' => true, 'tema' => null]);
    }
}