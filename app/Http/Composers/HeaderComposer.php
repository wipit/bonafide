<?php

namespace App\Http\Composers;

use Illuminate\View\View;
use App\Repositories\UserRepository;

class HeaderComposer
{
    protected $header;
    protected $slides;
    protected $titulo;

    public function __construct()
    {
        $ruta = \Request::route()->getName();
        $this->header = $ruta != 'enviar' ? $ruta : 'contacto';
        $slider = \App\Slider::with(['slides' => function($q){
            $q->where('visible', 1);
        }])->where('seccion', $this->header)->first();
        $this->slides = $slider->slides;
        $this->titulo = $slider->titulo;
    }

    public function compose(View $view)
    {
        $view->with(['slides' => $this->slides, 'header' => $this->header, 'titulo' => $this->titulo]);
    }
}