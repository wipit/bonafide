<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model {
	public function slides() {
        return $this->hasMany('App\Slide')->orderBy('orden', 'ASC');
    }
    public function visibles() {
        return $this->hasMany('App\Slide')->where('visible', 1);
    }
}
