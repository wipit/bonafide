<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index');
    $router->resource('/sliders', SlidersController::class);
    $router->resource('/destacados', DestacadosController::class);
    $router->resource('/categorias', CategoriasController::class);
    $router->resource('/productos', ProductosController::class);
    $router->resource('/localidades', LocalidadesController::class);
    $router->resource('/locales', LocalesController::class);
    $router->resource('/novedades', NovedadesController::class);
    $router->resource('/orden', OrdenController::class);
});
