<?php

namespace App\Admin\Controllers;

use App\Destacado;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class DestacadosController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Destacados')
            ->description('Listado')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Destacados')
            ->description('Detalle')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Destacados')
            ->description('Edición')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Destacados')
            ->description('Nuevo')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Destacado);
        $grid->model()->orderBy('orden', 'asc');
        $grid->disablePagination();
        $grid->disableFilter();
        $grid->disableExport();
        // $grid->disableCreation();
        $grid->disableRowSelector();
        $grid->orden();
        $grid->imagen()->image(env('APP_URL').'/uploads/', 100, 100);
        $grid->titulo('Título');
        $grid->link();
        $grid->visible()->display(function ($visible) {
            return $visible ? '<i class="fa fa-lg fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>';
        });
        $grid->actions(function ($actions) {
            // $actions->disableDelete();
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Destacado::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Destacado);
        $form->textarea('titulo', 'Título');
        $form->image('imagen')->move('destacados');
        $form->text('link');
        $form->number('orden');
        $form->select('visible')->options([1 => 'Sí', 0 => 'No'])->default(1);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });
        return $form;
    }
}
