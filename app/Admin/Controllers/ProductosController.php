<?php

namespace App\Admin\Controllers;

use App\Producto;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ProductosController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Productos')
            ->description('Listado')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Productos')
            ->description('Detalle')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Productos')
            ->description('Edición')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Productos')
            ->description('Nuevo')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Producto);
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->categoria('Categoría')->display(function($categorias) {
            $string = '';
            foreach($categorias as $categoria) {
                $string .= '<span class="badge bg-red">'.$categoria['nombre'].'</span>';
            }
            return $string;
        //     $categoria = \App\Categoria::find($categoria);
        //     return $categoria ? $categoria->nombre : 'Sin categoría';->badge()
        });
        // $grid->categoria()->pluck('nombre')->map('ucwords')->implode('<br/>');
        $grid->nombre()->sortable();
        $grid->descripcion('Descripción')->limit(40);
        $grid->presentaciones()->display(function($presentaciones) {
            $string = '';
            foreach($presentaciones as $presentacion) {
                $string .= $presentacion['presentacion'].' ';
            }
            return $string;
        });
        $grid->imagen()->image(env('APP_URL').'/uploads/', 100, 100);
        $states = [
            'on'  => ['value' => 1, 'text' => 'Sí', 'color' => 'primary'],
            'off' => ['value' => 0, 'text' => 'No', 'color' => 'default'],
        ];
        $grid->home('Destacado en home')->switch();
        $grid->destacado('Destacado en productos')->switch();
        $grid->visible()->switch();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->equal('categoria_id', 'Categoría')->select(\App\Categoria::orderBy('nombre', 'asc')->get()->pluck('nombre', 'id'));
            $filter->like('nombre', 'Nombre');
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Producto::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Producto);
        $form->text('nombre');
        $form->image('imagen')->move('productos');
        $form->textarea('descripcion', 'Descripción');
        $form->multipleSelect('categoria', 'Categoría')->options(\App\Categoria::orderBy('nombre', 'asc')->get()->pluck('nombre', 'id'));
        $form->hasMany('presentaciones', function (Form\NestedForm $form) {
            $form->text('presentacion', 'Presentación');
        });
        $states = [
            'on'  => ['value' => 1, 'text' => 'Sí', 'color' => 'primary'],
            'off' => ['value' => 0, 'text' => 'No', 'color' => 'default'],
        ];
        // $form->select('home', 'Destacado en home')->options([1 => 'Sí', 0 => 'No'])->default(0);
        // $form->select('destacado', 'Destacado en productos')->options([1 => 'Sí', 0 => 'No'])->default(0);
        // $form->select('visible')->options([1 => 'Sí', 0 => 'No'])->default(1);
        $form->switch('home', 'Destacado en home')->options($states);
        $form->switch('destacado', 'Destacado en productos')->options($states);
        $form->switch('visible')->options($states);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });
        return $form;
    }
}
