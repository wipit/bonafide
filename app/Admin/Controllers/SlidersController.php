<?php

namespace App\Admin\Controllers;

use App\Slider;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SlidersController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Encabezados')
            ->description('Secciones')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Encabezados')
            ->description('Detalle')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $slider = \App\Slider::find($id);
        return $content
            ->header('Encabezados')
            ->description($slider->seccion)
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Encabezados')
            ->description('Nuevo')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Slider);
        $grid->disableCreateButton();
        $grid->disablePagination();
        $grid->disableFilter();
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->seccion('Sección')->display(function ($seccion) {
            return ucfirst($seccion);
        });
        $grid->titulo('Título');
        $grid->visibles('Slides')->display(function ($slides) {
            $count = count($slides);
            return "<span class='label label-warning'>{$count}</span>";
        });
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Slider::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Slider);
        // $form->display('seccion', 'Sección');
        // $form->tab('Título', function ($form) {
            $form->textarea('titulo', 'Título');
            // $form->select('ubicacion', 'Ubicación')->options(['izquierda' => 'Izquierda', 'derecha' => 'Derecha']);
            // $form->select('altura')->options(['abajo' => 'Abajo', 'medio' => 'Medio']);
        // })->tab('Fondos', function ($form) {
            $form->hasMany('slides', function (Form\NestedForm $form) {
                $form->image('desktop', 'Imagen desktop')->move('slides');
                $form->image('mobile', 'Imagen mobile')->move('slides');
                $form->number('orden');
                $form->select('visible')->options([1 => 'Sí', 0 => 'No'])->default(1);
                $form->text('link')->help('Ej: http://www.google.com');
                $form->select('blank', 'Abrir link en nueva pestaña')->options([1 => 'Sí', 0 => 'No'])->default(0);
            });
        // });
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });
        return $form;
    }
}
