<?php

namespace App\Admin\Controllers;

use App\Producto;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Tree;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Controllers\ModelForm;

class OrdenController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Orden de productos');
            $content->body(Producto::tree(function ($tree) {
                $tree->disableCreate();
                $tree->branch(function ($branch) {
                    $src = '/uploads/' . $branch['imagen'] ;
                    $logo = "<img src='$src' style='max-width:40px; max-height:40px; margin-right: 10px;' class='img'/>";
                    return $logo.$branch['nombre'];
                });
            }));
        });
    }

    

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Orden de productos')
            ->description('Detalle')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Orden de productos')
            ->description('Edición')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Orden de productos')
            ->description('Nuevo')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Producto);

        $grid->id('Id');
        $grid->nombre('Nombre');
        $grid->imagen('Imagen');
        $grid->descripcion('Descripcion');
        $grid->home('Home');
        $grid->destacado('Destacado');
        $grid->visible('Visible');
        $grid->parent_id('Parent id');
        $grid->order('Order');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Producto::findOrFail($id));

        $show->id('Id');
        $show->nombre('Nombre');
        $show->imagen('Imagen');
        $show->descripcion('Descripcion');
        $show->home('Home');
        $show->destacado('Destacado');
        $show->visible('Visible');
        $show->parent_id('Parent id');
        $show->order('Order');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Producto);
        $form->text('nombre', 'Nombre');
        $form->number('order', 'Orden');
        return $form;
    }
}
