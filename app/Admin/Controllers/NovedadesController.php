<?php

namespace App\Admin\Controllers;

use App\Novedad;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class NovedadesController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Novedades')
            ->description('Listado')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Novedades')
            ->description('Detalle')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Novedades')
            ->description('Edición')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Novedades')
            ->description('Nueva')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Novedad);
        $grid->model()->orderBy('created_at', 'desc');
        $grid->disableFilter();
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->thumbnail()->image(env('APP_URL').'/uploads/', 100, 100);
        $grid->titulo('Título');
        $grid->bajada();
        $grid->contenido()->display(function($contenido){
            return substr(strip_tags($contenido), 0, 280);
        });
        $grid->imagen()->image(env('APP_URL').'/uploads/', 100, 100);
        $grid->visible()->switch();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Novedad::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Novedad);
        $form->image('thumbnail')->move('novedades');
        $form->text('titulo', 'Título');
        $form->text('bajada');
        $form->image('imagen')->move('novedades');
        $form->ckeditor('contenido');
        $states = [
            'on'  => ['value' => 1, 'text' => 'Sí', 'color' => 'primary'],
            'off' => ['value' => 0, 'text' => 'No', 'color' => 'default'],
        ];
        $form->switch('visible')->options($states)->default(1);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });
        return $form;
    }
}
