<?php

namespace App\Admin\Controllers;

use App\Local;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class LocalesController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Locales')
            ->description('Listado')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Locales')
            ->description('Detalle')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Locales')
            ->description('Edición')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Locales')
            ->description('Nuevo')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Local);
        $grid->model()->orderBy('localidad_id', 'asc');
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->localidad()->display(function($localidad) {
            return $localidad['localidad'];
        });
        $grid->nombre()->sortable();
        $grid->direccion('Dirección');
        $grid->tipo()->display(function($tipo) {
            return ucfirst($tipo);
        });
        $grid->telefono('Teléfono')->sortable();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->equal('localidad_id', 'Localidad')->select(\App\Localidad::orderBy('localidad', 'asc')->get()->pluck('localidad', 'id'));
            $filter->like('nombre', 'Nombre');
            $filter->scope('cafeteria', 'Cafetería')->where('tipo', 'cafeteria');
            $filter->scope('tradicional', 'Tradicional')->where('tipo', 'tradicional');
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Local::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Local);
        $form->text('nombre');
        $form->select('localidad_id', 'Localidad')->options(\App\Localidad::orderBy('localidad', 'asc')->get()->pluck('localidad', 'id'));
        $form->text('direccion', 'Dirección');
        $form->select('tipo')->options(['cafeteria' => 'Cafetería', 'tradicional' => 'Tradicional']);
        $form->text('telefono', 'Teléfono');
        $form->map('latitud', 'longitud', 'Coordenadas')->useGoogleMap();
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });
        return $form;
    }
}
