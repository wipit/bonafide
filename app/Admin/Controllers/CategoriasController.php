<?php

namespace App\Admin\Controllers;

use App\Categoria;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CategoriasController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Categorías')
            ->description('Árbol')
            ->body($this->tree());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Categorías')
            ->description('Detalle')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Categorías')
            ->description('Edición')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Categorías')
            ->description('Nueva')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Categoria);
        $grid->disableFilter();
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->nombre();
        $grid->productos()->display(function ($productos) {
            $count = count($productos);
            return "<span class='label label-warning'>{$count}</span>";
        });
        $grid->visible()->display(function ($visible) {
            return $visible ? '<i class="fa fa-lg fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>';
        });
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });
        return $grid;
    }

    protected function tree() {
        return Categoria::tree(function ($tree) {
            $tree->branch(function ($branch) {
                return $branch['visible'] ? $branch['nombre'].' <span style="color: green;">(visible)</span>' : $branch['nombre'].' <span style="color: red;">(oculto)</span>';
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Categoria::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Categoria);
        $form->text('nombre');
        $form->hidden('slug');
        $form->select('visible')->options([1 => 'Sí', 0 => 'No'])->default(1);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });
        $form->saving(function (Form $form) {
            $form->slug = str_slug($form->nombre);
            $form->input('slug', $form->slug);
        });
        return $form;
    }
}
