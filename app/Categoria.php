<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;

class Categoria extends Model {
	use ModelTree, AdminBuilder;

	public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->setTitleColumn('nombre');
    }

	public function productos() {
        return $this->belongsToMany('App\Producto');
    }
}
