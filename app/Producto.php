<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;

class Producto extends Model {
    use ModelTree, AdminBuilder;

	public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->setTitleColumn('nombre');
    }

	public function presentaciones() {
        return $this->hasMany('App\Presentacion');
    }

    public function categoria() {
        return $this->belongsToMany('App\Categoria');
    }
}
