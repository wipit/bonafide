<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactoRecibido extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $path;

    public function __construct($request, $path)
    {
        $this->data = $request;
        $this->path = $path;
    }    

    public function build()
    {
        if($this->path)
            return $this->markdown('emails.contacto')->attach($this->path, [
                'as' => 'cv.pdf',
                'mime' => 'application/pdf',
            ]);
        else
            return $this->markdown('emails.contacto');
    }
}
