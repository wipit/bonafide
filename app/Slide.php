<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model {
	protected $guarded = [];

	public function slider() {
        return $this->belongsTo('App\Slider');
    }
}
