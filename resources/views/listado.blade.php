<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						@foreach($productos as $producto)
							<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
								<div class="img-container">
									<img src="{{asset('uploads/'.$producto->imagen)}}" alt="" class="img-responsive">
								</div>
								@foreach($producto->categoria as $categoria)
									<h5 class="product-category">{{$categoria->nombre}}</h5>
								@endforeach
								<h4 class="product-name">
									{{$producto->nombre}}
									<?php $string = ''; ?>
									@foreach($producto->presentaciones as $presentacion)
										<?php $string .= ' '.$presentacion->presentacion.' -'; ?>
									@endforeach
									{{rtrim($string, '-')}}
								</h4>
								@if($producto->descripcion)
									<p class="parrafo">{{$producto->descripcion}}</p>
								@endif
							</div>
						@endforeach
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>