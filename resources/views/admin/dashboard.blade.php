<style>
    .title {
        font-size: 50px;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;
        display: block;
        text-align: center;
        margin: 20px 0 10px 0px;
    }

    .links {
        text-align: center;
        margin-bottom: 20px;
    }

    .links li {
        display: inline-block;
    }

    .links .fa {
        display: none;
    }

    .links a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    #app li.treeview > a {
        display: none;
    }

    #app li.treeview ul {
        padding: 0;
    }
</style>

<div class="title">
    Bienvenido, {!! Admin::user()->name !!}
</div>
<div class="links">
    <?php echo $__env->renderEach('admin::partials.menu', Admin::menu(), 'item'); ?>
</div>