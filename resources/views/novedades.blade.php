@extends('layout.app')

@section('content')				
	<div class="Novedades-cuerpo wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
		<div class="container">
			<div class="row">
				@foreach($novedades as $novedad)
					<div class="col-md-3 col-sm-6 col-xs-12 novedad-item">
						<a data-fancybox="novedad" data-type="iframe" href="{{route('novedad', $novedad->id)}}" class="leer-mas-novedades">
							<img src="{{asset('uploads/'.$novedad->thumbnail)}}" alt="" class="img-responsive">
							<div class="texto">
								<h3>{{str_limit($novedad->bajada, 90, '...')}}</h3>
								<span>Ver más +</span>
							</div>
						</a>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endsection