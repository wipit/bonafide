<html>
<head>
	<meta charset="utf-8">
	<title>Novedades</title>
	<link href="https://fonts.googleapis.com/css?family=Signika:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lightbox.css') }}?3" />
</head>
<body>
	<button data-fancybox-close="" class="fancybox-button fancybox-button--close" id="cerrar" title="Cerrar"></button>
	<div class="container container-nota">
		<div class="row nota">
			<div class="col-xs-12"><h1>{{$novedad->titulo}}</h1></div>
			<div class="col-xs-12"><p class="claro">{{$novedad->bajada}}</p></div>
			@if($novedad->imagen)
				<div class="col-md-4 col-sm-12">
					<img src="{{asset('uploads/'.$novedad->imagen)}}" class="img-responsive">
				</div>
				<div class="col-md-8 col-sm-12">
			@else
				<div class="col-md-12 col-sm-12">
			@endif
				{!! $novedad->contenido !!}
				</div>
		</div>
	</div>
	<script src="{{ asset('js/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script>
		$(document).ready(function(){
			$('#cerrar').click(function(){
				parent.jQuery.fancybox.getInstance().close();
			});
		});
	</script>
	</body>
</html>