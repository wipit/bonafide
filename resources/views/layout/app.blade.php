<?php
    $ruta = \Request::route()->getName();
    $titulo = $ruta == 'index' ? 'Bienvenido' : ucfirst($ruta);
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="Distribution" content="Global" />
        <meta name="Robots" content="all, index, follow" />
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="title" content="{{ $titulo }} - Bonafide" />
        <title>{{ $titulo }} - Bonafide</title>
        <link rel="shortcut icon" href="{{ asset('images/favicon.html') }}">
        <link rel="apple-touch-icon-precomposed" href="asset(images/ico/apple-touch-icon-57-precomposed.html') }}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="asset(images/ico/apple-touch-icon-72-precomposed.html') }}">  
        <link rel="icon" href="{{ asset('images/favicons/favicon.ico') }}" type="image/x-icon" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/stylesd28d.css?v=1.9') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive30f4.css?v=3') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/locales.css') }}" />
        <link href="https://fonts.googleapis.com/css?family=Signika:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
        <!-- JAVASCRIPT -->
        <!--[if lt IE 9]>
              <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body class="j-n">
        <body class="SeccionHome" id="home-bonafide">
            <style>
                #preloader {position:fixed;top:0;left:0;right:0;bottom:0;background-color:#fff;z-index:999999;}
                #status {width:207px;height:207px;position:absolute;left:50%;top:50%;background-image:url({{ asset('css/images/loading.gif') }});background-repeat:no-repeat;background-position:center top;margin:-105px 0 0 -105px;}
            </style>
            <div id="preloader"><div id="status"></div></div>
            @include('partials.nav')
            <section class="Wrapper">
                @yield('content')
                @include('partials.footer')
            </section>
        </div>
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142156041-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-142156041-1');
        </script>

        <script src="{{ asset('js/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.fancybox.js') }}"></script>
        <script src="{{ asset('js/preloader.js') }}"></script>
        <script src="{{ asset('js/wow.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/localization/messages_es.min.js') }}"></script>
        @yield('scripts')
        <script src="{{ asset('js/init.js') }}"></script>
    </body>
</html>
