@extends('layout.app')

@section('content')		
	<div class="Nosotros-Historia">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="Nosotros-Historia-Historia"  id="nosotros-historia">
						<div class="Nosotros-Historia-Historia-Imagen wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
							<img class="img-responsive" src="images/nosotros/historia-camiones-bonafide.jpg" alt="Camiones Bonafide">
						</div>
						<h2 class="TitleN1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">Historia</h2>
						<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">Bonafide comienza en 1917, cuando Geraldo Trinks, hijo de importadores de café decidió abrir un kiosco en el Pasaje Güemes, en pleno centro porteño. Allí se dedicó a la venta de café y se instaló la primera máquina tostadora del país. Aquel café nuevo, entero, sin baño de azúcar y accesible a todos los bolsillos se abrió camino, y tal fue el éxito del emprendimiento que para poder recibir a la gran cantidad de clientes Trinks abrió un segundo local en la misma galería, al que sumó la venta de caramelos. Geraldo sintió la necesidad de darle un nombre a su marca, que fuera reflejo de tradición y confianza. Así surgió Bonafide (buena fe), que ha sido desde entonces símbolo y bandera de la empresa.</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="Nosotros-Historia-Mercado" id="nosotros-mercado">
						<div class="Nosotros-Historia-Mercado-Imagen wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
							<img class="img-responsive" src="images/nosotros/mercado-bonafide.jpg" alt="Camiones Bonafide">
						</div>
						<h2 class="TitleN1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">Mercado</h2>
						<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">Bonafide es una empresa en continuo movimiento que se adapta y anticipa a las demandas de los consumidores. La tradicional empresa argentina elabora y comercializa más de 200 productos de fuerte arraigo en la memoria emotiva del consumidor argentino. Desde 2003, también cuenta con una gran cantidad de locales innovados para recibir a sus prestigiosos clientes bajo un concepto que reúne productos de la más alta calidad y los más ricos sabores con un ambiente cálido y moderno donde poder disfrutarlos. Bonafide es hoy líder en la elaboración y comercialización de café, bombones y chocolates, extendiendo su red en más de 240 locales en Argentina, Chile y Uruguay.</p>
						<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
							Además de su desarrollo en franquicias, Bonafide cuenta con otras unidades de negocio: el canal de venta directa a <a href="{{route('negocios')}}">empresas y petroleras</a>. 
						</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12">
				</div>
			</div>
		</div>
	</div>			
	<div class="Nosotros-Vision wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s" id="vision">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="VisionsBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s">
						<div class="BlockInfo-nosotros-vision">
							<h3 class="Title TitleN1">Visión</h3>
							<span class="Line"></span>
							<p class="Parrafo">Ser reconocidos por la calidad de nuestro café y chocolates, en un lugar donde se viven gratos momentos.</p>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
	<div class="Nosotros-Valores" id="valores">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="Nosotros-Valores-Header">
						<h2 class="Title TitleN1  wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">Valores</h2>
						<ul class="ListadoValores">
							<li class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
								<i class="ValorIcon Icon1"><img src="images/nosotros/icon-1.png"></i>
								<p class="ValorText">Valoramos a las personas y las respetamos integralmente. </p>
							</li>
							<li class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s">
								<i class="ValorIcon Icon2"><img src="images/nosotros/icon-2.png"></i>
								<p class="ValorText">Privilegiamos siempre la conducta honesta y nos comprometemos profundamente con la Compañía.</p>
							</li>
							<li class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.4s">
								<i class="ValorIcon Icon3"><img src="images/nosotros/icon-3.png"></i>
								<p class="ValorText">Administramos los recursos con sobriedad y eficiencia. </p>
							</li>
							<li class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.6s">
								<i class="ValorIcon Icon4"><img src="images/nosotros/icon-4.png"></i>
								<p class="ValorText">Sentimos pasión por el trabajo bien hecho y por lo que hacemos. </p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>	
@endsection