@extends('layout.app')

@section('content')		
	@include('includes/destacados')			
	<div class="Home-Productos">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 visible-lg visible-md visible-sm">
					<div class="banner-producto">
						<img src="{{ asset('images/home/banner-productos.jpg') }}" alt="" class="img-responsive">
						<div class="texto-producto-banner">
							<h5>nuestros productos</h5>
							<p>Encontrá la línea completa de productos Bonafide</p>
							<a href="{{route('productos')}}" class="btn btn-default">ver todos</a>
						</div>
					</div>
				</div>
				@if(count($productos))
					<div class="col-xs-12 col-sm-6 col-md-9" >
						<div class="row">
							<div id="owl-carousel-products" class="owl-carousel owl-theme">
								@foreach($productos as $producto)
									<div class="item">
										<div class="img-container">
											<img src="{{ asset('uploads/'.$producto->imagen) }}" alt="" class="img-responsive">
										</div>
										@foreach($producto->categoria as $categoria)
											<h5 class="product-category">{{$categoria->nombre}}</h5>
										@endforeach
										<h4 class="product-name">
											{{$producto->nombre}}
											<?php $string = ''; ?>
											@foreach($producto->presentaciones as $presentacion)
												<?php $string .= ' '.$presentacion->presentacion.' -'; ?>
											@endforeach
											{{rtrim($string, '-')}}
										</h4>
									</div>
								@endforeach
							</div>
						</div>	
					</div>
				@endif
			</div>
		</div>
	</div>			
	<div class="Franquicias wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="FranquiciasBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
						<div class="BlockInfo">
							<h3 class="Title TitleN1"><a href="{{ route('negocios') }}">franquicias</a></h3>
							<span class="Line"></span>
							<p class="Parrafo" style="text-align: justify;">Entrá y enterate qué ventajas brinda tener una franquicia Bonafide. Además de ser un negocio sólido, rentable, y perdurable en el tiempo a través de la venta de gran variedad de productos de alta calidad comercializados a nivel internacional.</p>
						</div>
					</div>
					<div class="BuscaLocalesBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s">
						<div class="BlockInfo">
							<h3 class="Title TitleN1"><a href="{{ route('locales') }}">buscá locales bonafide</a></h3>
							<span class="Line"></span>
							<p class="Parrafo">Conocé los locales de Bonafide de todo el país, tanto cafeterías como tradicionales.</p>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
@endsection