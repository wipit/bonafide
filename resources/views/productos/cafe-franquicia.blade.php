<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/fran_superior.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café Franquicia</h5>
							
							<h4 class="product-name"> Superior</h4>
							<p class="parrafo">Blend Colombiano con un tostado natural tipo italiano. Sabor fuerte con un aroma fragante y delicado. Ideal para tomar solo.</p>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/fran_seleccion.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café Franquicia</h5>
							
							<h4 class="product-name"> Selección</h4>
							<p class="parrafo">Blend Colombiano y Brasileño, con un tostado natural tipo suizo. Sabor delicado y suave con un aroma profundo, fino y persistente. Ideal para tomar solo.</p>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/fran_express.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café Franquicia</h5>
							
							<h4 class="product-name"> Express</h4>
							<p class="parrafo">Blend de Brasil con un tostado natural tipo suizo. Sabor delicado, profundo y pronunciado con un aroma fuerte y persistente. Para tomar solo o con leche.</p>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/fran_descafeinado.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café Franquicia</h5>
							
							<h4 class="product-name"> Descafeinado</h4>
							<p class="parrafo">Blend Colombiano con un tostado natural tipo italiano. Sabor delicado y suave,con un aroma fino y persistente. Para tomar solo o con leche.</p>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/fran_franjablanca.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café Franquicia</h5>
							
							<h4 class="product-name"> Franja Blanca</h4>
							<p class="parrafo">Blend de Colombia y Brasil con un tostado natural tipo suizo y un torrado español. Sabor fino, fuerte y natural con un aroma fino, pronunciado y fuerte. Ideal para tomar solo o con leche.</p>
						</div>	
					
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/fran_cintaazul.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café Franquicia</h5>
							
							<h4 class="product-name"> Cinta Azul</h4>
							<p class="parrafo">Blend de Colombia y Brasil con un tostado natural tipo suizo y torrado español. Sabor delicado con un aroma persistente. Ideal para tomar con leche.</p>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/fran_noir.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café Franquicia</h5>
							
							<h4 class="product-name"> Noir</h4>
							<p class="parrafo">Blend de Brasil con un torrado tipo español. Sabor y aroma muy fuerte. Para tomar con leche.</p>
						</div>
						
						
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/fran_fluminense.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café Franquicia</h5>
							
							<h4 class="product-name"> Fluminense</h4>
							<p class="parrafo">Blend de Brasil con un tostado natural tipo suizo y un torrado español. Sabor y aroma fuerte. Ideal para tomar solo o con leche.</p>
						</div>
						
						
						
										
						
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>