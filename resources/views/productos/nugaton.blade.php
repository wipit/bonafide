<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_rolls.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Rolls 55g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_rolls_display.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Rolls 10u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_mini48.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Mini 48g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_mini_display.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Mini 10u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_blanco.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Blanco 27g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_blanco24.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Blanco 24u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_blanco_tripack.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Blanco 3u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_black.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Black 27g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_black24.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Black 24u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_black_tripack.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Black 3u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_leche.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Leche 27g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_leche24.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Leche 24u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_familiar.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Leche 12u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_leche_27g.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Leche 3u</h4>
						</div>	
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_snack.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Snack 8.5g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/nu_snack_display.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Nugaton</h5>
							
							<h4 class="product-name"> Nugaton Snack 30u</h4>
						</div>
						
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>