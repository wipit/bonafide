<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bb-moneda.html" alt="" </a>
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Moneda Dolar 224g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bb-min.html" alt="" </a>
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Bombón Mint 158g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bb-gif.html" alt="" </a>
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Bombón Almendra 158g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bb-cher.html" alt="" </a>
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Bombón Cher 135g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bb-acr-almendra.html" alt="" </a>
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Bombón Acrílico Almendras 158g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bb-acr-cher.html" alt="" </a>
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Bombón Acrílico Cher 158g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bb-crem-alm.html" alt="" </a>
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Crema de Almendras 150g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bb-min-150.html" alt="" </a>
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Mint 150g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bb-milk-150.html" alt="" </a>
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Milk 150g</h4>
						</div>							
						
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>