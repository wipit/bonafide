<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/mecano456.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Mecano</h5>
							
							<h4 class="product-name"> Display Mecano 24u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/mecano35.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Mecano</h5>
							
							<h4 class="product-name"> Mecano 19g</h4>
						</div>

					</div>
				</div>	
			</div>
		</div>
	</div>
</div>