<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/panchips.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Pan Dulce</h5>
							
							<h4 class="product-name"> Chips de Chocolate 480g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/panfruta.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Pan Dulce</h5>
							
							<h4 class="product-name"> Con Frutas 480g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/panpremium.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Pan Dulce</h5>
							
							<h4 class="product-name"> Premium 1K</h4>
						</div>
					
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/budinchips250.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Budines</h5>
							
							<h4 class="product-name"> Chips de Chocolate 250G</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/budinlimon250.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Budines</h5>
							
							<h4 class="product-name"> Limón 250g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/budinmarmolado250.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Budines</h5>
							
							<h4 class="product-name"> Marmolado 250g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/turroncro100.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Turrones</h5>
							
							<h4 class="product-name"> Crocantes de Almendras 100g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/turroncremona120.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Turrones</h5>
							
							<h4 class="product-name"> Cremona de Almendras 120g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/turronsambayon120.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Turrones</h5>
							
							<h4 class="product-name"> Sambayon bañado en chocolate 120g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/turronchoc100.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Turrones</h5>
							
							<h4 class="product-name"> Chocolate con Maní 100g</h4>
						</div>
						
						
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/turronfruta120.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Turrones</h5>
							
							<h4 class="product-name"> Yema con Fruta 100g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/turronmani100.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Turrones</h5>
							
							<h4 class="product-name"> Crocante de Maní 100g</h4>
						</div>		
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/turronmanicremona100.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Turrones</h5>
							
							<h4 class="product-name"> Cremona de Maní con Miel 200g</h4>
						</div>			
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/turronmanifrutascremona100.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Turrones</h5>
							
							<h4 class="product-name"> Maní con Frutas 100g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/turronmanimiel100.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Turrones</h5>
							
							<h4 class="product-name"> Cremona de Maní con Miel 100g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/garra.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Garrapiñadas</h5>
							
							<h4 class="product-name"> Garrapiñada 100g</h4>
						</div>					
						
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>