<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/alf-negro.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Alfajores</h5>
							
							<h4 class="product-name"> Alfajor Chocolate 50g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/alf-blanca.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Alfajores</h5>
							
							<h4 class="product-name"> Alfajor Franja Blanca 50g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/alf-blanco.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Alfajores</h5>
							
							<h4 class="product-name"> Alfajor Blanco 50g</h4>
						</div>
						<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/bloc-almendras.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bloquecitos</h5>
							
							<h4 class="product-name"> Almendras 16g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/bloc-semi.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bloquecitos</h5>
							
							<h4 class="product-name"> Bitter 16g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/bloc-blanco.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bloquecitos</h5>
							
							<h4 class="product-name"> Blanco 16g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/cafetorrado250g.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café</h5>
							
							<h4 class="product-name"> Sensaciones Torrado Intenso 1kg - 500g - 250g - 125g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/cafesuave250.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café</h5>
							
							<h4 class="product-name"> Sensaciones Torrado Suave 1kg - 500g - 250g - 125g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/cafeintensosaquitos100g.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café</h5>
							
							<h4 class="product-name"> Sensaciones Torrado Intenso en Saquitos</h4>
						</div>
						<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/bb-moneda.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Moneda Dolar 224g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/bb-min.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Bombón Mint 158g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="{{ asset('images/productos/bb-gif.jpg') }}" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bombones</h5>
							
							<h4 class="product-name"> Bombón Almendra 158g</h4>
						</div>

					</div>
				</div>	
			</div>
		</div>
	</div>
</div></div></div>