<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/donuts_bitter.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Donuts</h5>
							
							<h4 class="product-name"> Donuts Bitter 78g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/donuts_blanco.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Donuts</h5>
							
							<h4 class="product-name"> Donuts Blanco 78g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/donuts_leche.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Donuts</h5>
							
							<h4 class="product-name"> Donuts Leche 78g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/donuts_26g.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Donuts</h5>
							
							<h4 class="product-name"> Donuts Leche 26g</h4>
						</div>
						

					</div>
				</div>	
			</div>
		</div>
	</div>
</div>