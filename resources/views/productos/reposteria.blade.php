<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/repo-semi.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Repostería</h5>
							
							<h4 class="product-name"> Baño de Repostería Semiamargo 150g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/repo-chips.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Repostería</h5>
							
							<h4 class="product-name"> Chips de Chocolate 150g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/repo-taza.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Repostería</h5>
							
							<h4 class="product-name"> Chocolate Taza 150g</h4>
						</div>

					</div>
				</div>	
			</div>
		</div>
	</div>
</div>