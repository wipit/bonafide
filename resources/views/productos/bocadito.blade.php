<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bocaditorhum16.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bocaditos</h5>
							
							<h4 class="product-name"> Al Rhum 16g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bocaditorhum24x16.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bocaditos</h5>
							
							<h4 class="product-name"> Al Rhum 24u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bocaditoleche16.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bocaditos</h5>
							
							<h4 class="product-name"> Dulce de Leche 16g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bocaditoleche24x16.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bocaditos</h5>
							
							<h4 class="product-name"> Dulce de Leche 12u</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bocaditolechex3u.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bocaditos</h5>
							
							<h4 class="product-name"> Dulce de Leche 5u</h4>
						</div>
						
						
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>