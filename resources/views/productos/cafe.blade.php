<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/cafetorrado250g.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café</h5>
							
							<h4 class="product-name"> Sensaciones Torrado Intenso 1kg - 500g - 250g - 125g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/cafesuave250.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café</h5>
							
							<h4 class="product-name"> Sensaciones Torrado Suave 1kg - 500g - 250g - 125g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/cafeintensosaquitos100g.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café</h5>
							
							<h4 class="product-name"> Sensaciones Torrado Intenso en Saquitos</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/cafegranotostado.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café</h5>
							
							<h4 class="product-name"> Café en Grano 500g - 1k</h4>
						</div>
						<div style="clear: both"></div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/cafenegrotostado.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Café</h5>
							
							<h4 class="product-name"> Café en Grano 500g - 1k</h4>
						</div>						
						
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>