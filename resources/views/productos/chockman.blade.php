<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/chock35.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Chocman</h5>
							
							<h4 class="product-name"> Chocman 35g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/chock420.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Chocman</h5>
							
							<h4 class="product-name"> Chocman 12u</h4>
						</div>

					</div>
				</div>	
			</div>
		</div>
	</div>
</div>