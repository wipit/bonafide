<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/alf-negro.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Alfajores</h5>
							
							<h4 class="product-name"> Alfajor Chocolate 50g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/alf-blanca.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Alfajores</h5>
							
							<h4 class="product-name"> Alfajor Franja Blanca 50g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/alf-blanco.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Alfajores</h5>
							
							<h4 class="product-name"> Alfajor Blanco 50g</h4>
						</div>

					</div>
				</div>	
			</div>
		</div>
	</div>
</div>