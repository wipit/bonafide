<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bloc-almendras.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bloquecitos</h5>
							
							<h4 class="product-name"><a href="#"> Almendras 16g</a></h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bloc-semi.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bloquecitos</h5>
							
							<h4 class="product-name"><a href="#"> Bitter 16g</a></h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bloc-blanco.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bloquecitos</h5>
							
							<h4 class="product-name"><a href="#"> Blanco 16g</a></h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bloc-choc.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bloquecitos</h5>
							
							<h4 class="product-name"><a href="#"> Milk 16g</a></h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/bloc-arroz.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Bloquecitos</h5>
							
							<h4 class="product-name"><a href="#"> Copos de Arroz 16g</a></h4>
						</div>

					</div>
				</div>	
			</div>
		</div>
	</div>
</div>