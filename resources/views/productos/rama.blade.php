<div class="Home-Productos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="row">
					<div id="owl-carousel-products-2">
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/ramablanco180.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Rama</h5>
							
							<h4 class="product-name"> Rama Blanco 180g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/ramaleche180.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Rama</h5>
							
							<h4 class="product-name"> Rama Leche 180g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/ramaduo.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Rama</h5>
							
							<h4 class="product-name"> Rama Duo 180g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/ramamix.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Rama</h5>
							
							<h4 class="product-name"> Rama Mix 180g</h4>
						</div>
							<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/ramablanco48.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Rama</h5>
							
							<h4 class="product-name"> Rama Blanco 40g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/ramaleche48.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Rama</h5>
							
							<h4 class="product-name"> Rama Leche 40g</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/ramablancodisplay.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Rama</h5>
							
							<h4 class="product-name"> Rama Blanco Display</h4>
						</div>
						<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
							
							<div class="img-container">
								<img src="images/productos/ramaregulardisplay.html" alt="" class="img-responsive">
							</div>
							<h5 class="product-category">Familia Rama</h5>
							
							<h4 class="product-name"> Rama Regular Display</h4>
						</div>
						
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>