@extends('layout.app')

@section('content')				
	<div class="Contacto">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4">
					<div class="BlockInfo">
							<h3 class="Title TitleN1">con qué área querés comunicarte?</h3>
							<p class="Parrafo">Elegí un departamento</p>
							<span class="Line"></span>
							<form class="FormContactoSelect">
								<select name="" id="FormContactoSelect">
									<option value="">Seleccione Departamento</option>
									<option value="Atención al Cliente">Atención al Cliente</option>
									@if(0)
									<!-- <option value="Venta de Cajas Navideñas">Venta de Cajas Navideñas</option>
									<option value="Franquicia">Franquicia</option>
									<option value="Directo / Vending">Directo / Vending</option>
									<option value="Contacto">Contacto</option>
									<option value="Marketing">Marketing</option> -->
									@endif
									<option value="Recursos Humanos">Recursos Humanos</option>
									@if(0)
									<!-- <option value="Logística">Logística</option>
									<option value="Otros">Otros</option> -->
									@endif
								</select>
							 </form>
						</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8">
					<form class="form-horizontal contacto-form" id="contacto-bonafide" method="post" action="{{route('contacto')}}" enctype="multipart/form-data">
						<div class="row">
							{{csrf_field()}}
							<input type="hidden" name="asunto" id="asunto" value="">
							<div class="form-group">
								<label class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-9">
								  <input type="text" name="nombre" class="form-control" value="" required>
								</div>
							  </div>
							  <div class="form-group">
								<label class="col-sm-3 control-label">Apellido</label>
								<div class="col-sm-9">
								  <input type="text" name="apellido" class="form-control" value="" required>
								</div>
							  </div>
							  <div class="form-group">
						  	 	<label class="col-sm-4 control-label" style="text-align: center;">CÓD. ÁREA</label>
							  	 <div class="col-sm-2">
									<input type="text" name="cod" class="form-control" required>
								  </div>
								  <label class="col-sm-2 control-label">TELÉFONO</label>
								  <div class="col-sm-4">
									<input type="text" name="tel" class="form-control" required>
								  </div>
							  </div>
							  <div class="form-group">
								<label class="col-sm-3 control-label">EMAIL</label>
								<div class="col-sm-9">
								  <input type="email" name="mail" class="form-control" value="" required>
								</div>
							  </div>
							  <div class="form-group">
								<label class="col-sm-3 control-label">ADJUNTAR CV</label>
								<div class="col-sm-9">
								  <input type="file" name="cv" class="form-control" value="" accept=".pdf,.doc,.docx">
								</div>
							  </div>
							  <div class="form-group">
								<label class="col-sm-3 control-label">MENSAJE</label>
								<div class="col-sm-9">
								  <textarea name="mensaje" id="" cols="30" rows="10" class="form-control" required></textarea>
								</div>
							  </div>
							  <div class="form-group">
								<div class="col-sm-offset-3 col-sm-6">
								  <input id="cambiar" type="submit" value="Enviar" name="enviar" class="btn btn-default contacto btn-lg">
								</div>
							  </div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>			
	<div class="Contacto-Familia wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s" style="display: none;">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="VisionsBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s">
						<div class="BlockInfo-nosotros-vision">
							<h3 class="Title TitleN1">formá parte <br />de la familia bonafide</h3>
							<p class="Parrafo">Envianos un mail a: <a href="mailto:recursoshumanos@bonafide.com.ar">recursoshumanos@bonafide.com.ar</a> con tu CV adjunto.</p>
							<span class="Line"></span>
							<p class="Parrafo-link"></p>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		<?php if($tema) { ?>
			var seleccionado = "<?php echo $tema; ?>";
		<?php } else { ?>
			var seleccionado = "";
		<?php } ?>
		$(document).ready(function(){
			$('#FormContactoSelect option[value="'+seleccionado+'"]').prop('selected', true);
			$('#asunto').val(seleccionado);
		});
		$("#cambiar").click(function(){
			if($('#contacto-bonafide').valid()) {
				$("#cambiar").val("enviando");
			}
		});
	</script>
	@if($enviado)
		<div id="trueModal" class="text-center" style="display: none; max-width: 600px; border: 1px solid #999; padding: 4em;">
	        <h2 class="text-center" style="margin-bottom: 1em;">
	            Recibimos tu consulta
	        </h2>
	        <p class="text-center" style="margin-bottom: 1em;">
	            Muy pronto nos comunicaremos con vos.
	        </p>
	        <p class="text-center">
	            <input type="button" data-fancybox-close="" class="m-auto btn btn-default contacto btn-lg" value="Cerrar" />
	        </p>
	    </div>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
		<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
		<a id="trigger" data-fancybox data-src="#trueModal" href="javascript:;"></a>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#trigger').click();
			});
		</script>
	@endif
@endsection