@extends('layout.app')

@section('content')		
	<div class="menu" id="locales-buscar">
		<select id="ubicacion" onchange="fillLocalidades();"></select>
		<select id="localidad">
			<option value="" disabled selected style="display: none;">Localidad</option>
		</select>
		<div class="submit" onclick="search();">Buscar</div>
	</div>		
	<div class="Locales-Mapa">
		<div id="map-canvas"></div>
		<div class="row" id="info-container" style="display: none;">
			<div class="container">
				<div class="col-md-5 col-sm-6 col-xs-12" id="map-imagen">
				imagen
				</div>
				<div class="col-md-7 col-sm-6 col-xs-12" id="map-texto">
					texto
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript"src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4p2u8dnFW_HBWPOC5zGVFE-Sus9awnHg"></script>
	<script src="{{ asset('js/locales.js') }}"></script>
	<script>
		var ubicacion = [
			<?php 
				$string = '';
				foreach($provincias as $provincia) {
					$string .= '["'.$provincia->provincia.'", "'.$provincia->alias.'","Argentina","'.$provincia->slug.'"],';
				}
				echo rtrim($string, ',');
			?>
		];
		var localidades = [
			<?php 
				$string = '';
				foreach($localidades as $localidad) {
					$string .= '["'.$localidad->provincia->provincia.', Argentina", "'.$localidad->localidad.'"],';
				}
				echo rtrim($string, ',');
			?>
		];
		var myInfo = [
			<?php 
				$string = '';
				foreach($locales as $local) {
					$string .= '["'.$local->direccion.'","'.$local->localidad->provincia->provincia.', Argentina","'.$local->localidad->localidad.'","img-local.jpg","'.$local->nombre.'","'.$local->tipo.'","'.$local->telefono.'"],';
				}
				echo rtrim($string, ',');
			?>
		];
		var myInfoLat = [
			<?php 
				$string = '';
				foreach($locales as $local) {
					$string .= '['.$local->latitud.','.$local->longitud.'],';
				}
				echo rtrim($string, ',');
			?>
		];
		$(window).load(function() {
			initialize();
		});
	</script>
@endsection