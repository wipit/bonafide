@extends('layout.app')

@section('content')	
	<div id="scroll_negocios" class="Negocios" style="padding: 10em 15px;">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h4>En construcción</h4>
				</div>
			</div>
		</div>
	</div>
@if(0)
	<div id="scroll_negocios" class="Negocios">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4">
					<div class="BlockInfo franquicias">
							
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<div class="BlockInfo petroleras">
							
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<div class="BlockInfo empresas">
						
					</div>
				</div>
			</div>
			<div id="scroll_to"></div>
			<div class="row tab-negocios tab-franquicias" style="margin: 5% 0">
				<div class="col-xs-12 col-sm-12 col-md-8">
					 <div class="contenido-tab">
							<h4 class="TitleN1 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.2s">¿Por qué una franquicia Bonafide?</h4>
							<p class="ParrafoBox wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.2s">
							La franquicia se presenta como un negocio sólido, rentable y perdurable en el tiempo a través de la venta de productos de la mejor calidad con presencia en el mercado internacional y la instalación de cafeterías donde los clientes pueden adquirir y degustar nuestros productos. Bonafide te brinda:
							</p>
							<ul class="lista">
								<li><i class="fa fa-circle" aria-hidden="true"></i> Imagen y diseño del local. En todos los casos Bonafide mediante un estudio de arquitectura suministra en forma exclusiva los diseños e imagen dentro y fuera del local.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> Entrenamiento inicial del personal. El entrenamiento del personal es obligatorio, se realiza antes y después de la apertura del local.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> Promociones y publicidad durante todo el año. Todos los locales y módulos participan de las promociones, publicidad y descuentos a nivel regional, provincial y nacional a cargo de Bonafide.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> Apoyo en la apertura Bonafide pone a disposición del franquiciado un equipo capacitado para asegurar desde el momento cero un negocio exitoso.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> Material de publicidad sin cargo. Todo el material de publicidad es entregado sin cargo en los locales.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> Manual operativo. Este manual contiene los conceptos teóricos y las herramientas que se necesitaran para la gestión del local.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> Asesoramiento comercial.</li>
							</ul>
							<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
							Los ejecutivos de cuenta asesoran, orientan en todos los aspectos relacionados a negocio y además controlan el cumplimiento de las pautas de la franquicia para maximizar el rendimiento del negocio.
							</p>
						</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4"><img src="{{asset('images/negocios/img-negocios.png')}}" class="img-responsive"></div>
			</div>
		  <div class="row tab-negocios tab-petroleras wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="col-xs-12 col-sm-12 col-md-8">
					<div class="contenido-tab">
						<h4 class="TitleN1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">¿Por qué una cafetería Bonafide?</h4>
						<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
							Propuesta de valor para el operador de estación:
						</p>
						<ul class="lista">
							<li><i class="fa fa-circle" aria-hidden="true"></i> Crecimiento rentable del negocio en forma sostenible.</li>
							<li><i class="fa fa-circle" aria-hidden="true"></i> Conocimiento y experiencia en Canal Petroleras.</li>
							<li><i class="fa fa-circle" aria-hidden="true"></i> Desarrollo de la Categoría Café dentro de la Estación.</li>
							<li><i class="fa fa-circle" aria-hidden="true"></i> Capacitación a baristas.</li>
						</ul>
						<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
							Propuesta de valor para el cliente final:
						</p>
						<ul class="lista">
							<li><i class="fa fa-circle" aria-hidden="true"></i> Mejora en la oferta de productos y servicios, optimizada para cada mercado.</li>
							<li><i class="fa fa-circle" aria-hidden="true"></i> Imagen, políticas y servicio homogéneos con atención diferenciada.</li>
							<li><i class="fa fa-circle" aria-hidden="true"></i> Mejora en confort, ambientación y luminosidad de las tiendas.</li>
							<li><i class="fa fa-circle" aria-hidden="true"></i> Degustación de nuestras diferentes variedades de café.</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4"><img src="{{asset('images/negocios/img-negocios.png')}}" class="img-responsive"></div>
			</div>
			<div class="row tab-negocios tab-empresas wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<div class="col-xs-12 col-sm-12 col-md-8">
					<div class="contenido-tab">
						<h4 class="TitleN1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">Bonafide en tu empresa</h4>
						<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
							El servicio de cafetería que tenés que tener en tu empresa. Bonafide División Empresas te ofrece la oportunidad de contar con un servicio único en máquinas expendedoras de bebidas y refrígerios.
						</p>
						<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
							Las máquinas Bonafide División Empresas se entregan en comodato y funcionan de acuerdo al pedido del cliente: con fichas, monedas de curso legal o libre acceso.
						</p>
						<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
							Además, Bonafide División Empresas te ofrece los siguientes servicios:
						</p>
					</div>
					<ul class="lista wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
						<li><i class="fa fa-circle" aria-hidden="true"></i> Combina la degustación y venta de un completo abanico de productos, permitiendo que día a día los clientes puedan encontrar distintas alternativas de consumo.</li>
						<li><i class="fa fa-circle" aria-hidden="true"></i> Pone a disposición del franquiciado un amplio surtido de productos, elaborado en plantas propias con altos estándares de calidad, y un programa de capacitación y supervisión para generar una adecuada operación del local.</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s"><img src="{{asset('images/negocios/img-negocios.png')}}" class="img-responsive"></div>
			</div>
		</div>
		<div class="Banner-bajo">
			<div class="container">
		     <!-- franquicias --> 
				    <div class="row wow fadeInUp franquicias-banner" data-wow-duration="1s" data-wow-delay="0.5s">
						<div class="col-xs-12">
							<h4 class="TitleN1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">Tipos de locales</h4>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<a href="{{asset('uploads/descargas/expresso.pdf')}}" target="_blank"><img src="{{asset('images/negocios/bonafide_expresso.png')}}" class="imagen"></a>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<a href="{{asset('uploads/descargas/modulo_cafe.pdf')}}" target="_blank"><img src="{{asset('images/negocios/bonafide_cafe.png')}}" class="imagen"></a>
							</div>
						</div>
				</div>	
			   <!-- petroleras --> 
				    <div class="row wow fadeInUp petrolera-banner" data-wow-duration="1s" data-wow-delay="0.5s">
						<div class="col-xs-12">
							<h4 class="TitleN1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">Ventajas competitivas</h4>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<ul class="lista">
									<li>• Un negocio sólido, rentable y perdurable en el tiempo.</li>
									<li>• La venta de productos de calidad con una fuerte presencia en el mercado.</li>
								</ul>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<ul class="lista">
									<li>• Ayuda a modificar la percepción femenina hacia la Estación.</li>
									<li>• Capta clientes no habituales a la Estación. Fideliza aún más a los clientes de la Estación.</li>
								</ul>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<ul class="lista">
									<li>• Clima ameno y tranquilo.</li>
									<li>• Garantía de buen café.</li>
								</ul>
							</div>
						</div>
				</div>	
			<!-- empresas -->
				  <div class="row wow fadeInUp empresas-banner" data-wow-duration="1s" data-wow-delay="0.5s">
						<div class="col-xs-12">
							<h4 class="TitleN1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">Ventajas</h4>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<ul class="lista">
									<li>• Se puede utilizar en cualquier momento del día y siempre esá listo.</li>
									<li>• Bajo costo de servicio.</li>
								</ul>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<ul class="lista">
									<li>• No se necesitan grandes espacios ni instalaciones complicadas.</li>
									<li>• Ahorro de tiempo por servicio.</li>
									<li>• Mayor comodidad.</li>
								</ul>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<ul class="lista">
									<li>• Recursos para refrigerio mejor administrados.</li>
									<li>• Los productos que se consumen los elige el cliente.</li>
								</ul>
							</div>
						</div>
				</div>	
			</div>
		</div>
		<div class="franquicias-bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4">
						<div class="BlockInfo">
							<a href="{{route('contacto', 'Franquicia')}}">
								<h3 class="Title TitleN1 text-white">Zonas <br>disponibles</h3>
								<span class="Line"></span>
								<p class="Parrafo">Consulte las zonas disponibles para acceder a su franquicia.</p>
							</a>
						</div>
						<div class="BlockInfo Marron">
							<a href="{{route('contacto')}}">
								<h3 class="Title TitleN1 text-white">Contáctese <br>con bonafide</h3>
								<span class="Line"></span>
								<p class="Parrafo">Complete el formulario con sus datos y nos pondremos en contacto a la brevedad.</p>
							</a>
						</div>
				 	</div>
					<div class="col-xs-12 col-sm-12 col-md-8">
						<div class="contenido-tab">
							<h4 class="TitleN1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">Ventajas<br>¿Por qué una franquicia Bonafide?</h4>
							<ul class="lista">
								<li><i class="fa fa-circle" aria-hidden="true"></i> Combina la degustación y venta de un completo abanico de productos, permitiendo que día a día los clientes puedan encontrar distintas alternativas de consumo.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> Pone a disposición del franquiciado un amplio surtido de productos, elaborado en plantas propias con altos estándares de calidad, y un programa de capacitación y supervisión para generar una adecuada operación del local.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> La imagen de marca es desarrollada a través de un plan anual del marketing, que incluye el diseño de vidrieras, promociones, publicidad y lanzamientos de nuevos productos para satisfacer y/o crear nuevas necesidades a los consumidores.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> La compañía de asesoramiento al franquiciado para facilitar el estudio sobre las posibilidades de un futuro local, y transfiere los lineamientos estéticos y funcionales para la construcción del mismo.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> Implica una inversión inicial ideal para emprendimientos particulares o familiares, logrando en poco tiempo el recupero de la misma.</li>
								<li><i class="fa fa-circle" aria-hidden="true"></i> Ofrece a los clientes la tradición de una compañía con más de 90 años en el mercado, en un ambiente agradable y cálido donde disfrutar el desayuno, el almuerzo y el after hour a solas o con amigas y amigos.</li>
							</ul>
							<p class="ParrafoBox wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
								Reconocida entre la opinión pública por los tradicionales locales de venta de café molido a la vista y por la calidad de sus productos, la compañía ha iniciado un plan de expansión dirigido a incrementar sus puntos de venta mediante el otorgamiento de nuevas franquicias.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
@endif
@endsection

@section('scripts')
	@if($preset)
		<script type="text/javascript">
			$(document).ready(function(){
				// $('html,body').animate({scrollTop: $('#scroll_negocios').offset().top}, 1000);
				setTimeout(function(){
					$('.BlockInfo.<?php echo $preset; ?>').click();
				}, 800);
			});
		</script>
	@endif
@endsection