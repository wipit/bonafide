<footer class="Footer">

	<div class="container">

		<div class="row">

			<div class="Footer-Box wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">

				<div class="Footer-Box-Logo">

					<img src="{{ asset('images/bonafide-logo-footer.png') }}" alt="Bonafide Logo">

					<ul class="RedesSociales">

						<li><a href="https://www.facebook.com/BonafideArgentina" target="_blank"><i class="fa fa-facebook"></i></a></li>

						<li><a href="https://twitter.com/BonafideArg" target="_blank"><i class="fa fa-twitter"></i></a></li>

						<li><a href="https://www.instagram.com/bonafidearg/" target="_blank"><i class="fa fa-instagram"></i></a></li>

						<li><a href="https://www.linkedin.com/company/16258100/" target="_blank"><i class="fa fa-linkedin"></i></a></li>

					</ul>

					<br />

					<div class="contacto-footer-datos">

						<div class="iconos"><a href="javascript:void(0);" style="cursor: default;"><i class="fa fa-phone"></i> 0800 444 2662</a></div>

						<div class="iconos"><a href="mailto:0800@bonafide.com.ar"><i class="fa fa-envelope"></i> 0800@bonafide.com.ar</a></div>

						<div class="iconos"><a href="https://bonafide.linea-etica.la" target="_blank"><i class="fa fa-exclamation"></i> Línea ética Bonafide</a></div>

					</div>

				</div>

			</div>

			<div class="Footer-Box-Menu wow fadeIn" data-wow-duration="1s" data-wow-delay="0s">

				<div class="Footer-Box-Menu-List wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">

					<h6>NOSOTROS</h6>

					<ul>

						<li><a href="{{route('nosotros')}}#nosotros-historia">HISTORIA</a></li>

						<li><a href="{{route('nosotros')}}#nosotros-mercado">MERCADO</a></li>

						<li><a href="{{route('nosotros')}}#vision">VISIÓN</a></li>

						<li><a href="{{route('nosotros')}}#valores">VALORES</a></li>

					</ul>

				</div>

				<div class="Footer-Box-Menu-List wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">

					<h6>LOCALES</h6>

					<ul>

						<li><a href="{{route('locales')}}#locales-buscar">Encontrá un local</a></li>

					</ul>

				</div>

				<div class="Footer-Box-Menu-List wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">

					<h6>PRODUCTOS</h6>

					<ul>

						<li><a href="{{route('productos')}}/cafe-franquicia">CAFÉ</a></li>

						<li><a href="{{route('productos')}}/chocolate">CHOCOLATES</a></li>

						<li><a href="{{route('productos')}}">OTROS</a></li>

					</ul>

				</div>

				<div class="Footer-Box-Menu-List wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">

					<h6>MUNDO BONAFIDE</h6>

					<ul>

						<li><a href="{{route('negocios')}}/franquicias">FRANQUICIAS</a></li>

						<li><a href="{{route('negocios')}}/petroleras">PETROLERAS</a></li>

						<li><a href="{{route('negocios')}}/empresas">EMPRESAS</a></li>

					</ul>

				</div>

				<div class="Footer-Box-Menu-List wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">

					<h6>CONTACTO</h6>

					<ul>

						<li><a href="{{route('contacto')}}">FORMULARIO DE CONTACTO</a></li>

					</ul>

				</div>

			</div>

		</div>

	</div>

</footer>