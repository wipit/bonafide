<header id="{{ \Request::route()->getName() == 'index' ? 'inicio' : '' }}">
	<div class="Header BgHeader-{{ucfirst($header)}} wow fadeIn <?php if(count($slides) < 1) { echo 'vacio'; } ?>" data-wow-duration="1s" data-wow-delay="0.5s">
		@if(count($slides) > 1)
			<div class="slider encabezado" data-multiple="{{ count($slides) > 1 ? 'true' : 'false' }}">
				@foreach($slides as $slide)
					<div class="slide">
						@if($slide->link != '')
						<a href="{{$slide->link}}" target="{{$slide->blank ? '_blank' : '_self'}}">
						@endif
						@if($slide->desktop)
							<img class="hidden-xs" src="{{asset('uploads/'.$slide->desktop)}}" />
						@endif
						@if($slide->mobile)
							<img class="hidden-sm hidden-md hidden-lg" src="{{asset('uploads/'.$slide->mobile)}}" />
						@else
							<img class="hidden-sm hidden-md hidden-lg" src="{{asset('uploads/'.$slide->desktop)}}" />
						@endif
						@if($slide->link != '')
						</a>
						@endif
					</div>
				@endforeach
			</div>
		@endif 
		@if(count($slides) == 1)
			<style type="text/css">
				@if($slides->first()->desktop)
					.Header {
						background-image: url({{asset('uploads/'.$slides->first()->desktop)}});
					}
				@endif
				@if($slides->first()->mobile)
					@media(max-width: 767px) {
						.Header {
							background-image: url({{asset('uploads/'.$slides->first()->mobile)}});
						}
					}
				@endif
			</style>
		@endif
		<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
			<ul class="RedesSociales">
				<li><a href="https://www.facebook.com/BonafideArgentina" target="_blank"><i class="fa fa-facebook"></i></a></li>
				<li><a href="https://twitter.com/BonafideArg" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<li><a href="https://www.instagram.com/bonafidearg/" target="_blank"><i class="fa fa-instagram"></i></a></li>
				<li><a href="https://www.linkedin.com/company/16258100/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			</ul>		
		</div>
		<div class="container">
			<div class="row">
				<div class="Header-Logo col-xs-12 col-sm-2 col-md-3 col-lg-2 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">						
					<div class="pull-left"><a href="{{route('index')}}"><img class="img-responsive LogoPrincipal" src="{{ asset('images/bonafide-logo.png') }}" alt="Bonafide"></a></div>
					<div class="icon-menu-mobile pull-right">
						<a href="javascript:void(0);"><img class="icon-menu-mobile" src="{{ asset('images/icon-menu-mobile.png') }}"></a>
					</div>
				</div>
				<div class="Header-Menu col-xs-12 col-sm-8 col-md-9 col-lg-10">
					<ul class="Menu-Acceso wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
						<li>
							<a href="{{route('locales')}}#locales-buscar"><i class="fa fa-map-marker"></i>BUSCAR LOCALES</a>
						</li>
						@if(0)
							<li>
								<a href="#" target="_blank" class="franquicias-abrir_"><i class="fa fa-lock"></i>LOGIN FRANQUICIAS</a>
								<a href="#" class="franquicias-cerrar-x"><i class="fa fa-lock"></i>LOGIN FRANQUICIAS</a>
								<ul class="sub-login">
									<li>
										<h6>ingrese un numero de franquicia</h6>
										<form id="login-franquicias" name="login-franquicias" method="post" action="http://www.bonafide.com.ar/datos/login.php">
											<input type="text" value="" name="nf" id="nf">
											<input type="submit" name="in" id="in" value="Entrar">
										</form>
									</li>
								</ul>
							</li>
						@endif
					</ul>
					<nav class="Menu-Principal hidden-xs wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s">
						<ul>
							<li class="{{ \Request::route()->getName() == 'index' ? 'active' : '' }}"><h3><a href="{{ route('index') }}">Inicio <span></span></a></h3></li>
							<li id="nosotros" class="{{ \Request::route()->getName() == 'nosotros' ? 'active' : '' }}"><h3><a href="{{ route('nosotros') }}">Nosotros <span></span></a></h3></li>
							<li id="locales" class="{{ \Request::route()->getName() == 'locales' ? 'active' : '' }}"><h3><a href="{{ route('locales') }}">Locales <span></span></a></h3></li>
							<li id="productos" class="{{ \Request::route()->getName() == 'productos' ? 'active' : '' }}"><h3><a href="{{ route('productos') }}">Productos <span></span></a></h3></li>
							<li id="negocios" class="{{ \Request::route()->getName() == 'negocios' ? 'active' : '' }}"><h3><a href="{{ route('negocios') }}">Negocios <span></span></a></h3></li>
							<li id="novedades" class="{{ \Request::route()->getName() == 'novedades' ? 'active' : '' }}"><h3><a href="{{ route('novedades') }}">Novedades <span></span></a></h3></li>
							<li id="contacto" class="{{ \Request::route()->getName() == 'contacto' ? 'active' : '' }}"><h3><a href="{{ route('contacto') }}">Contacto <span></span></a></h3></li>
						</ul>
					</nav>							
				</div>
				<div class="menu-mobile-open">
					<div class="cierra-menu"><a href="javascript:void(0);"><i class="fa fa-close"></i></a></div>
					<nav class="Menu-Principal">
						<ul>
							<li class="cierra-menu"><h3><a href="{{ route('index') }}">Inicio <span></span></a></h3></li>
							<li class="cierra-menu"><h3><a href="{{ route('nosotros') }}">Nosotros <span></span></a></h3></li>
							<li class="cierra-menu"><h3><a href="{{ route('locales') }}">Locales <span></span></a></h3></li>
							<li class="cierra-menu"><h3><a href="{{ route('productos') }}">Productos <span></span></a></h3></li>
							<li class="cierra-menu"><h3><a href="{{ route('negocios') }}">Negocios <span></span></a></h3></li>
							<li class="cierra-menu"><h3><a href="{{ route('novedades') }}">Novedades <span></span></a></h3></li>
							<li class="cierra-menu"><h3><a href="{{ route('contacto') }}">Contacto <span></span></a></h3></li>
						</ul>
					</nav>
				</div>
				@if($titulo && count($slides) > 0)			
					<div class="BlockInfo-{{$header}} wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s" style="visibility: visible; animation-duration: 1s; animation-delay: 1s; animation-name: fadeInUp;">
						<p class="Title TitleN1">{!!nl2br($titulo)!!}</p>
					</div>
				@endif		
			</div>
		</div>
		<div id="dots"></div>
	</div>
</header>