@extends('layout.app')

@section('content')		
	<div id="select-productos" class="Home-productos">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="Home-Conoce-Intro titulo">
						<div class="BlockInfo">
							<p class="Title TitleN1">Encontrá todos los productos </br><span class="Especial">en un solo lugar.</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="Home-Conoce-Intro">
						<div class="BlockInfo">
							<form action="" class="FormProductos">
								<select name="encontra" id="encontra">
									<option value="0">Seleccione Productos</option>
									@foreach($categorias as $categoria)
										<option value="{{$categoria->id}}" <?php echo $seleccionada && $categoria->id == $seleccionada->id ? 'selected="selected"' : '' ?>>{{$categoria->nombre}}</option>
									@endforeach
								</select>
							 </form>
						</div>	
					</div>			
				</div>
			</div>
		</div>
	</div>			
	<div id="los-productos">
		<div class="Home-Productos">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
						<div class="row">
							<div id="owl-carousel-products-2">
								@foreach($productos as $producto)
									<div class="producto owl-carousel-products-2 col-md-3 col-sm-6 col-xs-12">
										<div class="img-container">
											<img src="{{asset('uploads/'.$producto->imagen)}}" alt="" class="img-responsive">
										</div>
										@foreach($producto->categoria as $categoria)
											<h5 class="product-category">{{$categoria->nombre}}</h5>
										@endforeach
										<h4 class="product-name">
											{{$producto->nombre}}
											<?php $string = ''; ?>
											@foreach($producto->presentaciones as $presentacion)
												<?php $string .= ' '.$presentacion->presentacion.' -'; ?>
											@endforeach
											{{rtrim($string, '-')}}
										</h4>
										@if($producto->descripcion)
											<p class="parrafo">{{$producto->descripcion}}</p>
										@endif
									</div>
								@endforeach
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection