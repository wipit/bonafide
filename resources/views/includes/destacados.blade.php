@if(count($destacados)>0)
	<div class="Home-Conoce">
		<div class="container">
			<div class="row">
				@foreach($destacados as $i => $destacado)
					<div class="col-xs-12 col-sm-6 col-md-6 conoce">
						<div class="conoce-{{$i%2+1}}">
							<img src="{{ asset('uploads/'.$destacado->imagen) }}" class="img-responsive">
							<div class="texto">
								<h3>{{$destacado->titulo}}</h3>
								<a href="{{$destacado->link}}" class="btn btn-default">más info</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endif