<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">

    <label for="{{$id['lat']}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}}">

        @include('admin::form.error')

        <div id="map_{{$id['lat'].$id['lng']}}" style="width: 100%;height: 300px"></div>
        <label class="control-label">Latitud</label>
        <?php $latitud = $value['lat'] ? $value['lat'] : '-34.598030650543215'; ?>
        <input class="form-control" type="text" id="{{$id['lat']}}" name="{{$name['lat']}}" value="{{ old($column['lat'], $latitud) }}" {!! $attributes !!} />
        <label class="control-label">Longitud</label>
        <?php $longitud = $value['lng'] ? $value['lng'] : '-58.448638916015625'; ?>
        <input class="form-control" type="text" id="{{$id['lng']}}" name="{{$name['lng']}}" value="{{ old($column['lng'], $longitud) }}" {!! $attributes !!} />

        @include('admin::form.help-block')

    </div>
</div>
